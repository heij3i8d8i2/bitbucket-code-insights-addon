# **DEPRECATED** - This pipe will stop working after 1st May 2021. Instead, please use a more generic pipe available here [https://bitbucket.org/jfrog/jfrog-setup-cli/src/master/](https://bitbucket.org/jfrog/jfrog-setup-cli/src/master/).

# JFrog Artifactory and Xray Integration with the Atlassian Bitbucket Server Code Insights API #

This POC demonstrates that important information from JFrog Artifactory and Xray can be made avaialble on the BitBucket Pull Request screen as Bitbucket Insights Cards.

This integration is accomplished via a simple bash script that can be executed on the command line  or incorporated into build or post-build phase of CI servers.

## Prerequisites for this integration ##

1. Bitbucket Server with the support for the latest Bitbucket Code Insights API is installed and configured to manage git projects.

2. Build enviroment is configured with the project and repository created with appropriate  permissions for the user.

3. The repository is  cloned on a development machine where the code can be built and committed. (i.e git is configured, java, maven, jq, curl, bash are all avaialble for use).

4. Copy the contents of this POC to the root of the cloned git repo.

5. Artifactory is installed and an [Xray Watch](https://www.jfrog.com/confluence/display/XRAY/Watches) is configured with a target resource of a build you've setup in Artifactory. See [Artifactory Build Integration](https://www.jfrog.com/confluence/display/RTF/Build+Integration) for more information about configuring builds in Artifactory.

## How to use the POC ##

1. In the home directory of the cloned repository, download the POC files. 

2. Edit the file build.env and replace the values of all the environment variables in the file to match your environment.

3. Checkout some files and make some changes, commit and create a PR. 

4. You can now run "sh build.sh" to exercise the POC.

5. Login to BitBucket server, click on the PR, go to the overview section and click on the integrations and view the 2 cards. There will be one for Artifactory and one for XRay with additional information.

6. The links in the Artifactory and Xray cards have additional links that take you straight to the build info and xray scanning result details.
