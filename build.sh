#!/bin/bash
# This script requires 
# 	1) build.env, 
#	2) artbld.json.template
# 	3) xraybld.json.template
# 	4) config.yaml
# Assumptions: 
#	1) BitBucket with Insights API support setup and project and
# 	repo created and configured.
#	2) Artifactory and XRAY installed and configured.
#	3) build.env file has all values correctly setup.
#	4) config.yaml has the correct server id and maven repo details
#
# Source jbit.env which is expected to have all the required
# Artifactory, BitBucket and build related environment variables

. ./build.env

# We are using GIT_COMMIT in the Rest API call to BitBucket Server
# If it is not set in the CI, we will try to set the value here from git log
#
if [ -z "$GIT_COMMIT" ]; then
	githash=$(git log -n 1 | grep '^commit' | awk '{ print $2}')
	export GIT_COMMIT=${githash}
	echo "Commit Hash : $GIT_COMMIT"
fi
if [ -z "$GIT_BRANCH" ]; then
	gitbranch=$(git status | grep "On branch" | awk ' {print $3}')
	export GIT_BRANCH=${gitbranch}
	echo "Git Branch : $GIT_BRANCH"
fi

# CI_BUILD_NAME can be constructed based on CI defined environment
# variables like GIT_BRANCH, GIT_PROJECT, REPONAME etc
# CI_BUILD_NUM can also be obtained from CI specific environment variable

CI_BUILD_NAME="$REPONAME-$GIT_BRANCH"
CI_BUILD_NUM=`date '+%s'`

# Get JFrog CLI
curl -fL https://getcli.jfrog.io | sh
# configure artifactory
./jfrog rt config jf1  --url "$ARTIFACTORY_URL" --user "$ARTIFACTORY_USER" --apikey "$ARTIFACTORY_APIKEY" --interactive=false

# jfrog cli server name "jf1" has to match the serverID entry in config.yaml

./jfrog rt mvn "clean install" config.yaml --build-name=$CI_BUILD_NAME --build-number=$CI_BUILD_NUM

# read env vars
./jfrog rt bce "$CI_BUILD_NAME" "$CI_BUILD_NUM"

# Publish build info
./jfrog rt bp "$CI_BUILD_NAME" "$CI_BUILD_NUM" --server-id=jf1

# Get build info details for the build name and number
bldinfo=`curl -s -u $ARTIFACTORY_USER:$ARTIFACTORY_APIKEY -H "Accept: application/json" -H "Content-type: application/json" -X GET $ARTIFACTORY_URL/api/build/$CI_BUILD_NAME/$CI_BUILD_NUM`

artname=`echo  $bldinfo | jq -r .buildInfo.modules[0].artifacts[0].name`
artsha1=`echo  $bldinfo | jq -r .buildInfo.modules[0].artifacts[0].sha1`
artsha256=`echo  $bldinfo | jq -r .buildInfo.modules[0].artifacts[0].sha256`
artmd5=`echo  $bldinfo | jq -r .buildInfo.modules[0].artifacts[0].md5`
buildname=`echo  $bldinfo | jq -r .buildInfo.name`
buildnumber=`echo  $bldinfo | jq -r .buildInfo.number`
bldjsonlink=`echo  $bldinfo | jq -r .uri`



cdate=`date '+%s'`
# cardlink is the url in the ui
cardlink="$ARTIFACTORY_URL/webapp/#/builds/$CI_BUILD_NAME/$CI_BUILD_NUM/"
sed -e "s/CDATE/$cdate/g" \
       -e "s,LOGOURL,$ARTLOGOURL,g" \
       -e "s,BUILD_NAME,$buildname,g" \
       -e "s,BUILD_NUMBER,$buildnumber,g" \
       -e "s,ARTIFACT_NAME,$artname,g" \
       -e "s,ARTIFACT_SHA1,$artsha1,g" \
       -e "s,ARTIFACT_SHA256,$artsha256,g" \
       -e "s,BUILD_JSONLINK,$bldjsonlink,g" \
       -e "s,CARDLINK,$cardlink,g" artbld.json.template > artbld.json
curl -s -u ${BBUSER}:${BBPASSWORD} -H "Accept: application/json" -H "Content-type: application/json" --data @artbld.json -X PUT ${BBSERVER}:${BBPORT}/rest/insights/latest/projects/${PRJKEY}/repos/${REPONAME}/commits/${GIT_COMMIT}/reports/art-bldinfo-insights

# Send another card for XRAY Info

# Scan the build info artifacts in XRAY if configured
# if XRAY is not configured, this will fail after some retries, but this just
# results in a XRAY card not having any useful data

xscanresult=`./jfrog rt bs $CI_BUILD_NAME $CI_BUILD_NUM`
echo $xscanresult

fail_build="false"
summary_message="All is well"
more_details_url="$XRAY_URL"
total_alerts="0"
xraypassfail="PASS"

if [ ! -z "$xscanresult" ]; then
	echo "Get data from XRAY Scan .."
	fail_build=`echo $xscanresult | jq -r .summary.fail_build`
	summary_message=`echo $xscanresult | jq -r .summary.message`
	more_details_url=`echo $xscanresult | jq -r .summary.more_details_url`
	total_alerts=`echo $xscanresult | jq -r .summary.total_alerts`
fi

if [ -z "$more_details_url" ]; then
# xray might give an empty more_details_url
	more_details_url="$XRAY_URL"
else
	echo "Before More details_url = $more_details_url"
fi

if [ "$total_alerts" != "0" ]; then
	echo "Xray build has failed".
	xraypassfail="FAIL"
else
	echo "total_alerts is 0, so considering it as PASS"
fi
# If using orbitera, more_details_url might have "jfrog.local:8000", so the
# link may not work if there is no entry in /etc/hosts for jfrog.local

# Parse XRAY SCAN json output
sed -e "s/CDATE/$cdate/g" \
       -e "s,LOGOURL,$XRAYLOGOURL,g" \
       -e "s,SUMMARY_FAIL_BUILD,$fail_build,g" \
       -e "s/SUMMARY_MESSAGE/$summary_message/g" \
       -e "s,MORE_DETAILS_URL,$more_details_url,g" \
       -e "s,TOTAL_ALERTS,$total_alerts,g" \
       -e "s,PASS_OR_FAIL,$xraypassfail,g" \
       -e "s,CARDLINK,$XRAY_URL,g" xraybld.json.template > xraybld.json

curl -s -u ${BBUSER}:${BBPASSWORD} -H "Accept: application/json" -H "Content-type: application/json" --data @xraybld.json -X PUT ${BBSERVER}:${BBPORT}/rest/insights/latest/projects/${PRJKEY}/repos/${REPONAME}/commits/${GIT_COMMIT}/reports/xray-bldinfo-insights

